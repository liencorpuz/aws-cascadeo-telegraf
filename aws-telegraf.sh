#!/bin/bash

echo "===================================================="
echo "GLOBE DSG INFRASTRUCTURE"
echo "WELCOME! DSG INSTALLATION OF SEC-AGENTS"
echo "==================================================="
echo

#INSTANCE_IDINFO=$(cat ec2instanceid.txt)

if [ -f /etc/os-release ]; then
    . /etc/os-release
    OS=$NAME
    VER=$VERSION_ID
    ID_OS=$ID
    OS_VER="${VER:0:2}"
    OS_VER_AMZ="${VER:0:4}"
    echo $NAME "$OS_VER"

    if [ $ID == "centos" ] && [ $OS_VER -eq "7" ]
    then
        #######################################################################################
        #Download and Install Telegraf
        wget https://dl.influxdata.com/telegraf/releases/telegraf-1.18.1-1.x86_64.rpm
        sudo yum localinstall -y telegraf-1.18.1-1.x86_64.rpm; telegraf --input-filter disk --output-filter cloudwatch config > /tmp/telegraf_agent.conf
        sudo cp /tmp/telegraf_agent.conf /etc/telegraf/telegraf.conf
        sudo service telegraf stop
        echo "Copying telegraf.conf files...Please wait"
        sudo sleep 5
        sudo cp /tmp/secagents2021/aws-cascadeo-telegraf/telegraf.conf /etc/telegraf/telegraf.conf
        sudo service telegraf start
        sudo systemctl enable telegraf
        #######################################################################################

    fi

    if [ $ID == "ubuntu" ] && [ $OS_VER == "16" ]
    then
        #######################################################################################
        #Download and Install Telegraf
         wget https://dl.influxdata.com/telegraf/releases/telegraf_1.18.1-1_amd64.deb
        sudo dpkg -i telegraf_1.18.1-1_amd64.deb; telegraf --input-filter disk --output-filter cloudwatch config > /tmp/telegraf_agent.conf
        sudo cp /tmp/telegraf_agent.conf /etc/telegraf/telegraf.conf
        sudo service telegraf stop
        echo "Copying telegraf.conf files...Please wait"
        sudo sleep 5
        sudo cp /tmp/secagents2021/aws-cascadeo-telegraf/telegraf.conf /etc/telegraf/telegraf.conf
        sudo service telegraf start
        sudo systemctl enable telegraf
        #######################################################################################

    fi

       if [ $ID == "ubuntu" ] && [ $OS_VER == "18" ]
    then
         #######################################################################################
        #Download and Install Telegraf
         wget https://dl.influxdata.com/telegraf/releases/telegraf_1.18.1-1_amd64.deb
        sudo dpkg -i telegraf_1.18.1-1_amd64.deb; telegraf --input-filter disk --output-filter cloudwatch config > /tmp/telegraf_agent.conf
        sudo cp /tmp/telegraf_agent.conf /etc/telegraf/telegraf.conf
        sudo service telegraf stop
        echo "Copying telegraf.conf files...Please wait"
        sudo sleep 5
        sudo cp /tmp/secagents2021/aws-cascadeo-telegraf/telegraf.conf /etc/telegraf/telegraf.conf
        sudo service telegraf start
        sudo systemctl enable telegraf
        #######################################################################################

    fi

           if [ $ID == "ubuntu" ] && [ $OS_VER == "20" ]
    then
        #######################################################################################
        #Download and Install Telegraf
         wget https://dl.influxdata.com/telegraf/releases/telegraf_1.18.1-1_amd64.deb
        sudo dpkg -i telegraf_1.18.1-1_amd64.deb; telegraf --input-filter disk --output-filter cloudwatch config > /tmp/telegraf_agent.conf
        sudo cp /tmp/telegraf_agent.conf /etc/telegraf/telegraf.conf
        sudo service telegraf stop
        echo "Copying telegraf.conf files...Please wait"
        sudo sleep 5
        sudo cp /tmp/secagents2021/aws-cascadeo-telegraf/telegraf.conf /etc/telegraf/telegraf.conf
        sudo service telegraf start
        sudo systemctl enable telegraf
        #######################################################################################

    fi


    if [ $ID == "ubuntu" ] && [ $OS_VER == "14" ]
    then
         #######################################################################################
        #Download and Install Telegraf
         wget https://dl.influxdata.com/telegraf/releases/telegraf_1.18.1-1_amd64.deb
        sudo dpkg -i telegraf_1.18.1-1_amd64.deb; telegraf --input-filter disk --output-filter cloudwatch config > /tmp/telegraf_agent.conf
        sudo cp /tmp/telegraf_agent.conf /etc/telegraf/telegraf.conf
        sudo service telegraf stop
        echo "Copying telegraf.conf files...Please wait"
        sudo sleep 5
        sudo cp /tmp/secagents2021/aws-cascadeo-telegraf/telegraf.conf /etc/telegraf/telegraf.conf
        sudo service telegraf start
        sudo systemctl enable telegraf
        #######################################################################################

    fi


    if [ $ID == "amzn" ] && [ $OS_VER_AMZ == "2018" ]
    then
        #######################################################################################
        #Download and Install Telegraf
        wget https://dl.influxdata.com/telegraf/releases/telegraf-1.18.1-1.x86_64.rpm
        sudo yum localinstall -y telegraf-1.18.1-1.x86_64.rpm; telegraf --input-filter disk --output-filter cloudwatch config > /tmp/telegraf_agent.conf
        sudo cp /tmp/telegraf_agent.conf /etc/telegraf/telegraf.conf
        sudo service telegraf stop
        echo "Copying telegraf.conf files...Please wait"
        sudo sleep 5
        sudo cp /tmp/secagents2021/aws-cascadeo-telegraf/telegraf.conf /etc/telegraf/telegraf.conf
        sudo service telegraf start
        sudo systemctl enable telegraf
        #######################################################################################

    fi

    if [ $ID == "amzn" ] && [ $OS_VER_AMZ == "2015" ]
    then
        #######################################################################################
        #Download and Install Telegraf
        wget https://dl.influxdata.com/telegraf/releases/telegraf-1.18.1-1.x86_64.rpm
        sudo yum localinstall -y telegraf-1.18.1-1.x86_64.rpm; telegraf --input-filter disk --output-filter cloudwatch config > /tmp/telegraf_agent.conf
        sudo cp /tmp/telegraf_agent.conf /etc/telegraf/telegraf.conf
        sudo service telegraf stop
        echo "Copying telegraf.conf files...Please wait"
        sudo sleep 5
        sudo cp /tmp/secagents2021/aws-cascadeo-telegraf/telegraf.conf /etc/telegraf/telegraf.conf
        sudo service telegraf start
        sudo systemctl enable telegraf
        #######################################################################################

    fi

    if [ $ID == "amzn" ] && [ $OS_VER_AMZ == "2017" ]
    then
        #######################################################################################
        #Download and Install Telegraf
        wget https://dl.influxdata.com/telegraf/releases/telegraf-1.18.1-1.x86_64.rpm
        sudo yum localinstall -y telegraf-1.18.1-1.x86_64.rpm; telegraf --input-filter disk --output-filter cloudwatch config > /tmp/telegraf_agent.conf
        sudo cp /tmp/telegraf_agent.conf /etc/telegraf/telegraf.conf
        sudo service telegraf stop
        echo "Copying telegraf.conf files...Please wait"
        sudo sleep 5
        sudo cp /tmp/secagents2021/aws-cascadeo-telegraf/telegraf.conf /etc/telegraf/telegraf.conf
        sudo service telegraf start
        sudo systemctl enable telegraf
        #######################################################################################

    fi

    if [ $ID == "amzn" ] && [ $OS_VER == "2" ]
    then
        #######################################################################################
        #Download and Install Telegraf
        wget https://dl.influxdata.com/telegraf/releases/telegraf-1.18.1-1.x86_64.rpm
        sudo yum localinstall -y telegraf-1.18.1-1.x86_64.rpm; telegraf --input-filter disk --output-filter cloudwatch config > /tmp/telegraf_agent.conf
        sudo cp /tmp/telegraf_agent.conf /etc/telegraf/telegraf.conf
        sudo service telegraf stop
        echo "Copying telegraf.conf files...Please wait"
        echo "Copying telegraf.conf files...Please wait"
        sudo sleep 5
        sudo cp /tmp/secagents2021/aws-cascadeo-telegraf/telegraf.conf /etc/telegraf/telegraf.conf
        sudo service telegraf start
        sudo systemctl enable telegraf
        #######################################################################################

    fi




    if [ $ID == "rhel" ] && [ $OS_VER == "7" ]
    then
        #######################################################################################
        #Download and Install Telegraf
        wget https://dl.influxdata.com/telegraf/releases/telegraf-1.18.1-1.x86_64.rpm
        sudo yum localinstall -y telegraf-1.18.1-1.x86_64.rpm; telegraf --input-filter disk --output-filter cloudwatch config > /tmp/telegraf_agent.conf
        sudo cp /tmp/telegraf_agent.conf /etc/telegraf/telegraf.conf
        sudo service telegraf stop
        echo "Copying telegraf.conf files...Please wait"
        sudo sleep 5
        sudo cp /tmp/secagents2021/aws-cascadeo-telegraf/telegraf.conf /etc/telegraf/telegraf.conf
        sudo service telegraf start
        sudo systemctl enable telegraf
        #######################################################################################

        
    fi

fi

    echo
    echo "Completed..."

